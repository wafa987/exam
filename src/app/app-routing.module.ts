import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DomainComponent } from './domain/domain.component';
import { ChapterComponent } from './chapter/chapter.component';
import { Chapter1Component } from './chapters/chapter1/chapter1.component';
import { Chapter2Component } from './chapters/chapter2/chapter2.component';
import { Chapter3Component } from './chapters/chapter3/chapter3.component';
import { Chapter4Component } from './chapters/chapter4/chapter4.component';
import { Chapter5Component } from './chapters/chapter5/chapter5.component';
import { Chapter6Component } from './chapters/chapter6/chapter6.component';

const routes: Routes = [{ path: '', redirectTo: 'domain', pathMatch: 'full' },
  { path: 'domain', component: DomainComponent },
  {path: 'chapter', component: ChapterComponent},
  {path: 'chapter1', component: Chapter1Component},
  {path: 'chapter2', component: Chapter2Component},
  {path: 'chapter3', component: Chapter3Component},
  {path: 'chapter4', component: Chapter4Component},
  {path: 'chapter5', component: Chapter5Component},
  {path: 'chapter6', component: Chapter6Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

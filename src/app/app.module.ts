import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DomainComponent } from './domain/domain.component';
import { ChapterComponent } from './chapter/chapter.component';
import { Chapter1Component } from './chapters/chapter1/chapter1.component';
import { Chapter2Component } from './chapters/chapter2/chapter2.component';
import { Chapter3Component } from './chapters/chapter3/chapter3.component';
import { Chapter4Component } from './chapters/chapter4/chapter4.component';
import { Chapter5Component } from './chapters/chapter5/chapter5.component';
import { Chapter6Component } from './chapters/chapter6/chapter6.component';
import { CountdownModule } from 'ngx-countdown';

import { CdTimerModule } from 'angular-cd-timer';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    DomainComponent,
    ChapterComponent,
    Chapter1Component,
    Chapter2Component,
    Chapter3Component,
    Chapter4Component,
    Chapter5Component,
    Chapter6Component,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CountdownModule,
    CdTimerModule,
    NgbModule,

  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
